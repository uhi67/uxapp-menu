<?xml version="1.0" encoding="UTF-8"?>
<!-- menu.xsl -->
<!--suppress CheckTagEmptyBody -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.org/1999/xhtml">
	<xsl:output method="xml" version="1.0" indent="yes"
		doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
		doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
		omit-xml-declaration="yes"
	 />

	<!--
		Főmenü és almenük, breadcrumbs
	-->
	<xsl:template match="menu" mode="normal">
		<xsl:apply-templates select="." mode="menu-normal" />
	</xsl:template>

	<xsl:template match="*" mode="menu-normal" name="menu-normal">
		<xsl:param name="id" select="name()" />
		<xsl:param name="tooltip" select="''" />
		<xsl:param name="class" select="'mainmenu'" />

		<xsl:comment>Module menu</xsl:comment>
		<xsl:variable name="xtooltip"><xsl:if test="$tooltip!=''">uxapp-tooltip</xsl:if></xsl:variable>
		<div id="{$id}" class="menu {$xtooltip} inline clearfix">
			<xsl:if test="$tooltip!=''"><xsl:attribute name="data-title"><xsl:value-of select="$tooltip" /></xsl:attribute></xsl:if>
			<xsl:if test="attributes">
				<xsl:for-each select="attributes/@*">
					<xsl:attribute name="{name()}"><xsl:value-of select="." /></xsl:attribute>
				</xsl:for-each>
			</xsl:if>
			<xsl:if test="name()='breadcrumbs'">
				<span class="breadcrumbs-leader" title="Előzmények">
					<i class="fa fa-angle-double-right"></i>
				</span>
			</xsl:if>

			<ul class="menu {$class}">
				<xsl:if test="ul-attributes">
					<xsl:for-each select="ul-attributes/@*">
						<xsl:attribute name="{name()}"><xsl:if test="name()='class'">menu </xsl:if><xsl:value-of select="." /></xsl:attribute>
					</xsl:for-each>
				</xsl:if>
				<xsl:apply-templates select="item" mode="menu" />
			</ul>
		</div>
	</xsl:template>

	<!--
		(Listához kapcsolt) kontextus menü
		Attributumok:
			id: id értéke (default: generated)
			url: "a" tag url-je, ha nincs, akkor span lesz
			click: onclick esemény
			hint: title értéke
			block = 1 esetén blokkelem, 0 és üres esetén nem-blokk elem, egyébként állandó. Blokkelemek csak akkor jelennek meg, ha van kijelölt sor. Kezdetben láthatatlanok.
			autosubmit: autosubmit class, értéke data-act lesz (csak ha url nincs megadva)
			act: data-act beállítása autosumbit nélkül (csak ha url nincs megadva)
			tooltip: tooltip szöveg, értéke data-title lesz, nemüres esetén uxapp-tooltip class
			confirm
			*select
			*param
			*input
	-->
	<xsl:template match="menu" mode="context" name="menu-context">
		<xsl:param name="class" select="'context-menu-box'" />
		<xsl:param name="id" select="''" />
		<xsl:comment>menu context-menu</xsl:comment>
		<div id="{$id}" class="{$class}">
			<xsl:variable name="menu-id" select="generate-id()" />
			<ul class="context-menu menu {@class}" id="xmenu_{$menu-id}">
				<xsl:apply-templates select="item" mode="context-menu" />
			</ul>
		</div>
	</xsl:template>

	<!-- Ez a jobb, ebbe integráljuk a másikat -->
	<xsl:template match="item" mode="context-menu" name="menu-context-item">
		<xsl:variable name="item-id">
			<xsl:choose>
				<xsl:when test="@id!=''"><xsl:value-of select="@id" /></xsl:when>
				<xsl:otherwise><xsl:value-of select="generate-id()" /></xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="xblock">
			<xsl:choose>
				<xsl:when test="not(@block) or @block='' or @block='0'">nonblock_operation</xsl:when>
				<xsl:when test="@block='1'">block_operation</xsl:when>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="disabled"><xsl:if test="not(@enabled&gt;0)">disabled</xsl:if></xsl:variable>
		<xsl:variable name="autosubmit"><xsl:if test="@autosubmit">autosubmit</xsl:if></xsl:variable>

		<xsl:variable name="act">
			<xsl:choose>
				<xsl:when test="@autosubmit!=''"><xsl:value-of select="@autosubmit" /></xsl:when>
				<xsl:when test="@act"><xsl:value-of select="@act" /></xsl:when>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="tooltip"><xsl:if test="@tooltip">uxapp-tooltip</xsl:if></xsl:variable>
		<xsl:variable name="select"><xsl:if test="@select">select</xsl:if></xsl:variable>

		<xsl:variable name="title">
			<xsl:choose>
				<xsl:when test="@title!=''"><xsl:value-of select="@title" /></xsl:when>
				<xsl:otherwise>Megerősítés</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<li id="menu_{$item-id}">
			<xsl:if test="@li-class"><xsl:attribute name="class"><xsl:value-of select="@li-class" /></xsl:attribute></xsl:if>
			<xsl:choose>
				<xsl:when test="@url!='' and $disabled!='disabled'">
					<xsl:variable name="hidden"><xsl:if test="@block=1">hidden</xsl:if></xsl:variable>
					<a id="{$item-id}" href="{@url}" class="{$xblock} {$disabled} {$tooltip} {$hidden} {@class}" title="{@hint}">
						<xsl:if test="@target!=''"><xsl:attribute name="target"><xsl:value-of select="@target" /></xsl:attribute></xsl:if>
						<xsl:if test="@click!='' or @confirm!=''">
							<xsl:attribute name="onclick">
								<xsl:choose>
									<xsl:when test="@confirm and @click!=''">
										return UConfirm('<xsl:value-of select="$title" />', '<xsl:value-of select="@confirm" />',
											function() {
												if(<xsl:value-of select="@click" />)
													document.location = '{@url}';
											}
										);
									</xsl:when>
									<xsl:when test="@confirm">
										return UConfirm('<xsl:value-of select="$title" />', '<xsl:value-of select="@confirm" />',
											function() {
												document.location = '<xsl:value-of select="@url" />';
											}
										);
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="@click" />;
									</xsl:otherwise>
								</xsl:choose>
							</xsl:attribute>
						</xsl:if>
						<xsl:if test="@tooltip!=''"><xsl:attribute name="data-title"><xsl:value-of select="@tooltip" /></xsl:attribute></xsl:if>
						<xsl:if test="@hint!=''"><xsl:attribute name="title"><xsl:value-of select="@hint" /></xsl:attribute></xsl:if>
						<xsl:if test="@class!=''"><xsl:attribute name="class"><xsl:value-of select="@class" /></xsl:attribute></xsl:if>
						<xsl:apply-templates select="." mode="data" />
						<xsl:apply-templates select="." mode="caption" />
					</a>
				</xsl:when>
				<xsl:otherwise>
					<xsl:variable name="hidden"><xsl:if test="@block=1">hidden</xsl:if></xsl:variable>
					<span id="{$item-id}" class="{$xblock} {$disabled} {$autosubmit} {$tooltip} {$select} {$hidden} {@class}" title="{@hint}">
						<xsl:if test="@autosubmit and @confirm">
							<xsl:attribute name="data-confirm"><xsl:value-of select="@confirm" /></xsl:attribute>
							<xsl:if test="@click">
								<xsl:attribute name="data-click"><xsl:value-of select="@click" /></xsl:attribute>
							</xsl:if>
						</xsl:if>
						<xsl:if test="(@click!='' or @confirm!='') and not(@autosubmit)">
							<xsl:attribute name="onclick">
								<xsl:choose>
									<xsl:when test="@confirm">
										return UConfirm('<xsl:value-of select="$title" />', '<xsl:value-of select="@confirm" />',
											function() {
												<xsl:value-of select="@click" />;
											}
										);
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="@click" />
									</xsl:otherwise>
								</xsl:choose>
							</xsl:attribute>
						</xsl:if>
						<xsl:if test="not(@id!='') and @tooltip"><xsl:attribute name="id"><xsl:value-of select="generate-id()" /></xsl:attribute></xsl:if>
						<xsl:if test="$act!=''"><xsl:attribute name="data-act"><xsl:value-of select="$act"/></xsl:attribute></xsl:if>
						<xsl:if test="@tooltip!=''"><xsl:attribute name="data-title"><xsl:value-of select="@tooltip" /></xsl:attribute></xsl:if>
						<xsl:if test="@param"><xsl:attribute name="data-param"><xsl:value-of select="@param" /></xsl:attribute></xsl:if>
						<xsl:if test="@select"><xsl:attribute name="data-target"><xsl:value-of select="@select" /></xsl:attribute></xsl:if>
						<xsl:apply-templates select="." mode="data" />
						<xsl:apply-templates select="." mode="caption" />
					</span>
				</xsl:otherwise>
			</xsl:choose>
		</li>
		<!-- connected input (implicite selector buttons) -->
		<xsl:if test="@input">
			<input id="input_{$item-id}_id" type="hidden" name="{@input}" />
			<input id="input_{$item-id}_name" type="hidden" name="{@input}_value" />
		</xsl:if>
	</xsl:template>

	<xsl:template match="item" mode="menu" name="menu-item">
		<xsl:variable name="item-id" select="position()" />
		<li id="menu_{$item-id}">
			<xsl:if test="@li-class"><xsl:attribute name="class"><xsl:value-of select="@li-class" /></xsl:attribute></xsl:if>
			<xsl:choose>
				<xsl:when test="@enabled=0 or @enabled='' or not(@enabled) or @enabled='false' or @enabled='f' or (not(@url) and not(@click))">
					<span id="item_{$item-id}" class="disabled" data-click="{@click}">
						<xsl:if test="@hint!=''"><xsl:attribute name="title">A művelethez nincs jogod (<xsl:value-of select="@hint" />)</xsl:attribute></xsl:if>
						<xsl:apply-templates select="." mode="data" />
						<xsl:apply-templates select="." mode="caption" />
					</span>
				</xsl:when>
				<xsl:when test="@click or @url='#'">
					<span id="item_{$item-id}" onclick="{@click}">
						<xsl:if test="@hint!=''"><xsl:attribute name="title"><xsl:value-of select="@hint" /></xsl:attribute></xsl:if>
						<xsl:if test="@class!=''"><xsl:attribute name="class"><xsl:value-of select="@class" /></xsl:attribute></xsl:if>
						<xsl:apply-templates select="." mode="data" />
						<xsl:apply-templates select="." mode="caption" />
					</span>
				</xsl:when>
				<xsl:otherwise>
					<a href="{@url}">
						<xsl:if test="@target!=''"><xsl:attribute name="target"><xsl:value-of select="@target" /></xsl:attribute></xsl:if>
						<xsl:if test="@hint!=''"><xsl:attribute name="title"><xsl:value-of select="@hint" /></xsl:attribute></xsl:if>
						<xsl:if test="@class!=''"><xsl:attribute name="class"><xsl:value-of select="@class" /></xsl:attribute></xsl:if>
						<xsl:if test="@confirm!=''">
							<xsl:attribute name="onclick">
								var c = confirm('<xsl:value-of select="@confirm" />');
								if(!c) $(this).trigger('cancel-confirm');
								return c;
							</xsl:attribute>
						</xsl:if>
						<xsl:apply-templates select="." mode="data" />
						<xsl:apply-templates select="." mode="caption" />
					</a>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="name(..)='breadcrumbs' and position()!=last()">
				<i class="fa fa-angle-right"></i>
			</xsl:if>
		</li>
	</xsl:template>

	<xsl:template match="item" mode="caption">
		<xsl:if test="@icon"><i class="fa {@icon}"></i></xsl:if>
		<xsl:value-of select="@caption" />
	</xsl:template>

	<xsl:template match="item" mode="data">
		<xsl:for-each select="@*[substring(name(),1,5)='data-']">
			<xsl:copy-of select="." />
		</xsl:for-each>
	</xsl:template>

	<!-- Objektum címsorához kapcsolt képes menü -->
	<xsl:template match="menu" mode="title" name="menu-title">
		<xsl:param name="id" select="name()" />
		<xsl:param name="tooltip" select="''" />
		<xsl:param name="class" select="'menu-title'" />

		<xsl:comment>Module menu - title</xsl:comment>
		<xsl:variable name="xtooltip"><xsl:if test="$tooltip!=''">uxapp-tooltip</xsl:if></xsl:variable>
		<div id="{$id}" class="menu-title {$xtooltip} inline clearfix">
			<xsl:if test="$tooltip!=''"><xsl:attribute name="data-title"><xsl:value-of select="$tooltip" /></xsl:attribute></xsl:if>
			<ul class="menu {$class}">
				<xsl:apply-templates select="item" mode="menu" />
			</ul>
		</div>
	</xsl:template>

	<!-- *** navbar mode menu for bootstrap4 *** -->
	<xsl:template match="menu" mode="navbar">
		<xsl:apply-templates select="." mode="menu-navbar"/>
	</xsl:template>
	<xsl:template match="*" mode="menu-navbar">
		<xsl:variable name="class">
			<xsl:choose>
				<xsl:when test="@class"><xsl:value-of select="@class"/></xsl:when>
				<xsl:otherwise>navbar navbar-expand-md</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<nav class="{$class}">
			<xsl:apply-templates select="." mode="navbar-brand" />
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"/>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav navbar-right nav">
					<xsl:apply-templates select="item"/>
				</ul>
			</div>
		</nav>
	</xsl:template>

	<!-- callback -->
	<xsl:template match="callback-menu-navbar-brand" mode="navbar-brand" />

	<!-- *** menu within a bootstrap4 navbar *** -->
	<xsl:template match="menu" mode="nav">
		<xsl:comment>menu-bs4</xsl:comment>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav d-flex">
				<xsl:apply-templates select="item"/>
			</ul>
		</div>
	</xsl:template>

	<!-- disabled item in navbar -->
	<xsl:template match="menu/item[not(@enabled=1)]">
		<xsl:variable name="empty"><xsl:if test="not(@caption!='')">empty</xsl:if></xsl:variable>
		<li class="nav-item {@li-class} disabled">
			<xsl:if test="@hint"><xsl:attribute name="title"><xsl:value-of select="@hint"/></xsl:attribute></xsl:if>
			<a class="nav-link {@class} {$empty} disabled" href="#">
				<xsl:apply-templates select="@*[starts-with(name(), 'data-')]" mode="item-data"/>
				<xsl:if test="@target"><xsl:attribute name="target"><xsl:value-of select="@target"/></xsl:attribute></xsl:if>
				<xsl:if test="@icon"><i class="fa {@icon}"></i></xsl:if>
				<xsl:value-of select="@caption"/>
			</a>
		</li>
	</xsl:template>

	<!-- enabled item in navbar without submenu -->
	<xsl:template match="menu/item[@enabled=1 and not(item)]">
		<xsl:variable name="empty"><xsl:if test="not(@caption!='')">empty</xsl:if></xsl:variable>
		<li class="nav-item {@li-class}">
			<xsl:if test="@hint"><xsl:attribute name="title"><xsl:value-of select="@hint"/></xsl:attribute></xsl:if>
			<a class="nav-link {@class} {$empty}" href="{@url}">
				<xsl:apply-templates select="@*[starts-with(name(), 'data-')]" mode="item-data"/>
				<xsl:if test="@act"><xsl:attribute name="data-act"><xsl:value-of select="@act"/></xsl:attribute></xsl:if>
				<xsl:if test="@confirm">
					<xsl:attribute name="onclick">Uxapp.UConfirm('Megerősítés', '<xsl:value-of select="@confirm"/>', '<xsl:value-of select="@url"/>'); return false;</xsl:attribute>
				</xsl:if>
				<xsl:if test="@icon"><i class="fa {@icon}"></i></xsl:if>
				<xsl:value-of select="@caption"/>
			</a>
		</li>
	</xsl:template>

	<!-- enabled item with submenu in navbar -->
	<xsl:template match="menu/item[@enabled=1 and item]">
		<xsl:variable name="empty"><xsl:if test="not(@caption!='')">empty</xsl:if></xsl:variable>
		<xsl:variable name="item-id" select="generate-id(.)" />
		<li class="nav-item dropdown {@li-class}">
			<xsl:if test="@hint"><xsl:attribute name="title"><xsl:value-of select="@hint"/></xsl:attribute></xsl:if>
			<a class="nav-link dropdown-toggle {@class} {$empty}" href="#" role="button" id="button-{$item-id}" data-toggle="dropdown">
				<xsl:apply-templates select="@*[starts-with(name(), 'data-')]" mode="item-data"/>
				<xsl:if test="@icon"><i class="fa {@icon}"></i></xsl:if>
				<xsl:value-of select="@caption"/>
			</a>
			<div class="dropdown-menu" aria-labelledby="button-{$item-id}">
				<xsl:apply-templates select="item" />
			</div>
		</li>
	</xsl:template>

	<!-- disabled item in a submenu in navbar -->
	<xsl:template match="menu/item/item[not(@enabled=1)]">
		<xsl:variable name="empty"><xsl:if test="not(@caption!='')">empty</xsl:if></xsl:variable>
		<a class="dropdown-item disabled {@class} {$empty}" href="#" tabindex="-1">
			<xsl:apply-templates select="@*[starts-with(name(), 'data-')]" mode="item-data"/>
			<xsl:if test="@hint"><xsl:attribute name="title"><xsl:value-of select="@hint"/></xsl:attribute></xsl:if>
			<xsl:if test="@icon"><i class="fa {@icon}"></i></xsl:if>
			<xsl:value-of select="@caption"/>
		</a>
	</xsl:template>

	<!-- item in a submenu in navbar -->
	<xsl:template match="menu/item/item">
		<xsl:variable name="empty"><xsl:if test="not(@caption!='')">empty</xsl:if></xsl:variable>
		<a class="dropdown-item {@class} {$empty}" href="{@url}">
			<xsl:apply-templates select="@*[starts-with(name(), 'data-')]" mode="item-data"/>
			<xsl:if test="@act"><xsl:attribute name="data-act"><xsl:value-of select="@act"/></xsl:attribute></xsl:if>
			<xsl:if test="@hint"><xsl:attribute name="title"><xsl:value-of select="@hint"/></xsl:attribute></xsl:if>
			<xsl:if test="@target"><xsl:attribute name="target"><xsl:value-of select="@target"/></xsl:attribute></xsl:if>
			<xsl:if test="@icon"><i class="fa {@icon}"></i></xsl:if>
			<xsl:value-of select="@caption"/>
		</a>
	</xsl:template>

	<xsl:template match="@*" mode="item-data">
		<xsl:attribute name="{name()}"><xsl:value-of select="." /></xsl:attribute>
	</xsl:template>


</xsl:stylesheet>
