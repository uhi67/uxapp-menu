Menu Module for UXApp
=====================

Version 1.0 -- Released 2021-04-28

Installation
------------

Add to your `composer.json`:

    {
      "require": {
        "uhi67/uxapp-menu": "*"
      },
      "repositories": [
        {
          "type": "vcs",
          "url": "git@bitbucket.org:uhi67/uxapp-menu.git"
        }
      ],
    }

Usage
-----

### In the controller:

```
$this->register('Menu'); // in the preconstruct
$menu = new Menu($this, ['nodename'=>'otherthanmenu']);	// 1. Modules need a page 2. may override the default nodename
$menu->addItems(array(array('caption'=>'One')));
$menu->createNode($somenode);
```

or

```
$this->register('Menu'); // in the preconstruct
$menu = (new Menu($this, array('items'=>array(
        ['caption'=>'One', 'enabled'=>true, 'url'=>'ide'],
        ['caption'=>'Two'],
))))->createNode($somenode);
```

### In the view:

```
    <xsl:apply-templates select="menu" mode="normal">
        <xsl:with-param name="tooltip">Description for the menu.</xsl:with-param>
    </xsl:apply-templates>
```

Or, in a bootstrap4 navbar:

    <xsl:apply-templates select="menu" mode="nav">


## Menu properties:

- nodename (default is menu)
- items (may be added later by {@see addItems()} and {@see addItem()})
- options (creates attributes in XML -- not used)
- attributes (creates attributes in HTML div)
- ulAttributes -- attributes for menu node in XML -- not used

Change log
----------

### Version 1.0 -- Released 2021-04-28

- first stable release
