<?php /** @noinspection PhpUnused */

namespace uhi67\menu;

use uhi67\uxapp\ArrayUtils;
use uhi67\uxapp\Assertions;
use uhi67\uxapp\Module;
use uhi67\uxapp\Util;
use uhi67\uxapp\UXApp;
use uhi67\uxapp\UXAppException;
use uhi67\uxapp\UXAppPage;
use uhi67\uxml\UXMLDoc;
use uhi67\uxml\UXMLElement;

/** @noinspection PhpUnused */

/**
 * # Menu class
 *
 * Usage
 * -----
 * ### In the controller:
 * ```
 * $menu = new Menu(['page'=>$this, name'=>'mymenu1']);	// Modules need a page; multiple menus on a page must have unique names
 * $menu->addItems(array(array('caption'=>'One')));
 * $menu->createNode($somenode);
 * ```
 *
 * or
 *
 * ```
 * $menu = (new Menu(['page'=>$this, 'items'=>[
 *		['caption'=>'One', 'enabled'=>true, 'url'=>'ide'],
 *		['caption'=>'Two'],
 * ]]))->createNode($somenode);
 * ```
 *
 * ### In the view:
 * ```
 * 	<xsl:apply-templates select="menu[@name='mymenu1']" mode="menu-normal">
 *		<xsl:with-param name="tooltip">Description for the menu.</xsl:with-param>
 * 	</xsl:apply-templates>
 * ```
 *
 * ### Creating a bootstrap 4 navbar:
 *
 * ```
 * 		<xsl:apply-templates select="menu" mode="navbar" />
 * ```
 *
 * *For navbar brand use the callback:*
 *
 * ```
 * 		<xsl:template match="menu" mode="navbar-brand"> ... </xsl:template>
 * ```
 *
 * ## Menu properties:
 *
 * - page (the controller, mandatory)
 * - name (multiple menus on a page must have unique names)
 * - nodename (default is menu (do not change))
 * - items (may be added later by {@see addItems()} and {@see addItem()})
 * - options (creates attributes in XML -- not used)
 * - attributes (creates attributes in HTML div)
 * - ulAttributes -- attributes for menu node in XML -- not used
 * - class -- classname to nav item (default is "navbar navbar-expand-md", you may want to add them for custom values)
 *
 * ## item properties (elements of items array)
 *
 * - enabled,
 * - caption,
 * - url,
 * - hint,
 * - confirm
 * - class -> 'a class'
 * - li-class -> 'li class'
 * - autosubmit -> act
 * - act -> act
 * - tooltip = message
 * - block = only in list-context menus: 0 (non-block), 1 (block), 2 (fix) see BLOCK_XXX constants
 * - id -> 'a id'
 * - icon,
 * - items (recurse to submenu, only one level)
 * - data (associative array)
 * - any other keys
 *
 * url may be an array, then createUrl() will be applied.
 *
 * see {@see addItem} for menu item properties.
 */
class Menu extends Module {
    // Constants about displaying context menu items in cases of row selections
    const BLOCK_NO = 0; // hide in selections
    const BLOCK_YES = 1; // display only in selections
    const BLOCK_FIX = 2; // display always

    /** @var array $items -- the items added to menu */
    public $items = array();
    /** @var string $name -- unique names recommended for each menus on the page. Default is empty */
    public $name;
    /** @var string $nodename -- the XML nodename of the menu mode, default is 'menu' */
    public $nodename = 'menu';
    /** @var array $attributes -- the attributes for menu DIV or navbar*/
    public $attributes = array();
    /** @var array $options -- attributes for menu node in XML -- not used */
    public $options = array();
    /** @var string class -- class names for nav node */
    public $class;
    /** @var array $ulAttributes -- attributes for menu node in XML -- not used */
    public $ulAttributes = array();

    public function init() {
        parent::init();
        if($this->name) $this->name = Util::toNameID($this->name);
    }

    /**
     * Processes runtime actions when request contains act=module
     *
     * @return void
     */
    function action() {}

    /**
     * Creates the menu node in the page document
     *
     * @param UXMLElement|UXAppPage|Module $node_parent
     * @return UXMLElement the result node
     */
    function createNode($node_parent=null) {
        $node_parent = $this->parentNode($node_parent);
        /** @var UXMLDoc $doc */
        if(!(($doc = $node_parent->ownerDocument) instanceof UXMLDoc)) throw new UXAppException('Invalid parent');

        // Finds existing or creates menu node
        //$this->node = $node_parent->getOrCreateElement($this->nodename);
        $nameCondition = $this->name ? "[not(@name)]" : "[@name='$this->name']";
        $this->node = $node_parent->selectSingleNode($this->nodename.$nameCondition);
        if(!$this->node) $this->node = $node_parent->addNode($this->nodename, [
            'name' => $this->name,
            'class' => $this->class,
        ]);

        foreach($this->options as $name=>$value) $this->node->setAttribute($name, $value);
        if($this->attributes) {
            $node_attributes = $this->node->addNode('attributes');
            foreach($this->attributes as $name => $value) $node_attributes->setAttribute($name, $value);
        }
        if($this->ulAttributes) {
            $node_attributes = $this->node->addNode('ul-attributes');
            foreach($this->ulAttributes as $name => $value) $node_attributes->setAttribute($name, $value);
        }
        return $this->createItems();
    }

    /**
     * ## Adds an item to the menu
     *
     * Special values:
     * - add `@`target to url if a.target is needed
     * - add ##class to url if a.class is needed
     *
     * Data is a [key=>value...] array, like:
     * - enabled,
     * - caption,
     * - url,
     * - hint,
     * - confirm
     * - class -> 'a class'
     * - li-class -> 'li class'
     * - autosubmit -> act
     * - act -> act
     * - tooltip = message
     * - block = only in list-context menus: 0 (non-block), 1 (block), 2 (fix) see BLOCK_XXX constants
     * - id -> 'a id'
     * - icon,
     * - items (recurse to submenu, only one level)
     * - data (associative array)
     * - any other keys
     *
     * url may be an array, then createUrl() will be applied.
     *
     * @param array $data {enabled, caption, url, hint, confirm, ...}
     *
     * @return Menu
     */
    public function addItem($data) {
        if(!ArrayUtils::isAssociative($data, true)) throw new UXAppException('Menu item must be an associative array');
        $this->items[] = $data;
        return $this;
    }

    /** @noinspection PhpUnused */

    /**
     * ## Adds multiple items to the menu
     * See {@see addItem()} for details
     *
     * @param array $data -- array of item data: {enabled, caption, url, hint, confirm, attributes}
     * @return Menu
     */
    public function addItems($data) {
        $this->items = array_merge($this->items, $data);
        return $this;
    }

    /**
     * ## Generates the previously defined items in the last menu node
     *
     * Item data contains:
     * - enabled,
     * - caption,
     * - url -- the link of the menu item
     *    - normal string url
     *    - javascript string: click event will be applied
     *    - array: url generated by createUrl()
     * - hint,
     * - confirm,
     * - attributes (deprecated: all other keys are attributes)
     * - data (associative array)
     * - any other keys
     *
     * @return UXMLElement -- the menu node
     * @throws
     */
    private function createItems() {
        foreach($this->items as $item) $this->createItemNode($this->node, $item);
        return $this->node;
    }

    /**
     * Creates a node from item data
     *
     * @param UXMLElement $parentNode
     * @param array|null $item
     *
     * @return UXMLElement|null
     * @throws UXAppException
     */
    public function createItemNode($parentNode, $item) {
        if($item===null) return null;
        Assertions::assertArray($item);
        $itemNode = $parentNode->ownerDocument->createElement('item');
        $itemNode = $parentNode->appendChild($itemNode);
        $itemNode->setAttribute('enabled', $enabled = (ArrayUtils::fetchValue($item, 'enabled', null) ? 1 : 0));
        $icon = ArrayUtils::fetchValue($item, 'icon', null);
        $caption = ArrayUtils::fetchValue($item, 'caption', null);
        if($icon) $itemNode->setAttribute('icon', $icon);
        if($caption || !$icon) $itemNode->setAttribute('caption', $caption ?: 'undefined');

        $click = ArrayUtils::fetchValue($item, 'click', null);

        $url = ArrayUtils::fetchValue($item, 'url', '');
        if(is_array($url)) $url = UXApp::$app->createUrl($url);
        $jp = 'javascript:';
        if($url && substr($url, 0, strlen($jp))==$jp && !$click) {
            $click = substr($url, strlen($jp));
            $url = '';
        }
        if($click)
            $itemNode->setAttribute('click', $enabled ? $click : '');
        if($url) {
            if(strpos($url, '##')!==false) {
                list($url, $class) = explode('##', $url);
                $itemNode->setAttribute('class', $class);
            }
            if(strpos($url, '@')!==false) {
                list($url, $target) = explode('@', $url);
                $itemNode->setAttribute('target', $target);
            }
            $itemNode->setAttribute('url', $url);
        }

        $data = ArrayUtils::fetchValue($item, 'data');
        if(is_string($data)) $data = json_decode($data, true);
        if(is_array($data)) {
            foreach($data as $key=>$value) $itemNode->setAttribute('data-'.$key, $value);
            unset($item['data']);
        }

        if(isset($item['attributes'])) {
            foreach($item['attributes'] as $key=>$value) {
                if(!is_numeric($key)) $itemNode->setAttribute($key, $value);
            }
            unset($item['attributes']);
        }

        $items = ArrayUtils::fetchValue($item, 'items', []);
        foreach($items as $subitem) {
            $this->createItemNode($itemNode, $subitem);
        }

        // All other keys are attributes of item node.
        $itemNode->addAttributes($item);
        return $itemNode;
    }
}

